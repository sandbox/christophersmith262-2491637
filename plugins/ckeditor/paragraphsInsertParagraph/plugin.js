/**
 * @file
 *
 * A CKEDITOR Plugin for integrating with the paragraphs module.
 */

( function() {
  "use strict";

  // Set the text for the 'Add ...' button
  var singular = 'Paragraph';
  if (typeof Drupal.settings.paragraphsCkeditor !== 'undefined') {
    if (typeof Drupal.settings.paragraphsCkeditor.singular !== 'undefined') {
      singular = Drupal.settings.paragraphsCkeditor.singular;
    }
  }

  // Register CKEDITOR plugin
  CKEDITOR.plugins.add( 'paragraphsInsertParagraph', {
    requires : [ "dialog" ],
    init : function (editor) {
      editor.addCommand( 'paragraphsInsertParagraph', new CKEDITOR.dialogCommand('paragraphsInsertParagraphBundleSelector') );
      editor.ui.addButton( 'paragraphsInsertParagraph', {
        label: 'Add ' + singular,
        command: 'paragraphsInsertParagraph',
        toolbar: 'insert',
        icon: this.path + '/images/component.png',
      } );
      CKEDITOR.dialog.add( 'paragraphsInsertParagraphBundleSelector', this.path + 'dialogs/bundleSelector.js' );
    },
  } );

} )();
