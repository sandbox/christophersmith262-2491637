/**
 * @file
 *
 * A CKEDITOR dialog for selecting a paragraph bundle.
 */
"use strict";

CKEDITOR.dialog.add( 'paragraphsInsertParagraphBundleSelector', function ( editor ) {

  // Populate local helper variables
  var $form = jQuery(editor.container.$).parents('form');
  var $row = jQuery(editor.container.$).parents('.form-wrapper:first').parent();
  var $button = $row.find('.paragraphs-insert-trigger:last');
  var $field = jQuery(editor.container.$).parents('.paragraphs-insert-field:first');

  // Set up settings object
  var settings = {
    bundles: [],
    singular: 'Paragraph',
  };
  if (typeof Drupal.settings.paragraphsCkeditor !== 'undefined') {
    for (var key in Drupal.settings.paragraphsCkeditor) {
      settings[key] = Drupal.settings.paragraphsCkeditor[key];
    }
  }

  // Load supported bundles from the settings object.
  var bundles = new Array();
  var field_name = $button.attr('data-field');
  if (typeof settings.bundles !== 'undefined' && typeof settings.bundles[field_name] !== 'undefined') {
    for (var bundle in settings.bundles[field_name]) {
      var item = new Array();
      item.push(settings.bundles[field_name][bundle].name);
      item.push(bundle);
      bundles.push(item);
    }
  }

  /**
   * Gets bundle info based on its key name.
   */
  function lookupBundle(field, bundle) {
    var info = {
      bundle: '',
      name: '',
      ckeditor_description: '',
    };
    if (field in settings.bundles) {
      if (bundle in settings.bundles[field]) {
        for (var key in settings.bundles[field][bundle]) {
          info[key] = settings.bundles[field][bundle][key];
        }
      }
    }
    return info;
  }

  /**
   * Update the description 'element' based on which 'bundle' is selected.
   */
  function updateDescription(element, bundle) {
    var info = lookupBundle($button.attr('data-field'), bundle);
    var markup = '<i style="font-style: italic;">' + info.ckeditor_description + '</i>';
    jQuery('#' + element._.dialog.getContentElement('tab-basic', 'description').domId).html(markup);
  }

  /**
   * Splits a CKEDITOR.dom.text node into two nodes at offset.
   */
  function splitText(element, offset) {
    var left = element.clone(false);
    var right = element.clone(false);
    var source = element.getText();
    var split = {
      left: new String(),
      right: new String(),
    };
    var i;
    for (i = 0 ; i < source.length; i++) {
      if (i < offset) {
        split.left = split.left.concat(source.charAt(i));
      }
      else {
        split.right = split.right.concat(source.charAt(i));
      }
    }
    return split;
  }

  /**
   * Determines if a node is an empty container.
   */
  function isNodeEmpty(element) {
    var empty = true;
    if (typeof element !== 'undefined') {
      if (element.type === CKEDITOR.NODE_TEXT || element.getChildCount()) {
        empty = false;
      }
    }
    return empty;
  }

  /**
   * Pushes node to a list if the element is not empty.
   */
  function pushNodeElement(list, element) {
    if (!isNodeEmpty(element)) {
      list.push(element);
    }
  }

  /**
   * Reads a CKEDITOR.dom.* tree as an html string.
   */
  function readTree(root) {
    var html = '';
    if (root) {
      var body_list = root.getElementsByTag('body');
      if (body_list.count()) {
        html = body_list.getItem(0).getHtml();
      }
      return html;
    }
  }

  /**
   * Partitions a CKEDITOR dom tree based on a selection range.
   *
   * This will split the tree where the range *starts*.
   */
  function partitionTree(range) {
    var partitioned = {
      left: new String(),
      right: new String(),
    };

    // Set up generic lists for storing children to be added as we traverse
    // up the dom tree.
    //
    // The choice of data structure here corrosponds to the order in which we
    // read siblings from the canonical tree.
    var left_child_stack = new Array();
    var right_child_queue = new Array();

    // The beginning of the range is used as the starting point in the tree, we
    // build a left-hand tree by copying all nodes to the "left" of the pivot
    // node, and vice-versa for the right-hand tree.
    var pivot = range.startContainer;

    // If the range starts in a text node then we need to split the text apart,
    // otherwise we assume the pivot is an empty container element.
    if (typeof pivot !== 'undefined' && pivot.type === CKEDITOR.NODE_TEXT) {
      var split = splitText(pivot, range.startOffset);
      if (split.left.length) {
        left_child_stack.push(new CKEDITOR.dom.text(split.left));
      }
      if (split.right.length) {
        right_child_queue.push(new CKEDITOR.dom.text(split.right));
      }
      pivot = pivot.getParent();
    }

    // Traverse up the dom tree to build left-hand and right-hand partitions one
    // level at a time.
    while (pivot) {

      // Add children for the left-hand partition.
      var left_root = pivot.clone(false);
      var left;
      while (left = left_child_stack.pop()) {
        left_root.append(left);
      }

      // Add children for the right-hand partition.
      var right_root = pivot.clone(false);
      var right;
      while (right = right_child_queue.shift()) {
        right_root.append(right);
      }

      // Build a stack of children to add to left-hand partition.
      pushNodeElement(left_child_stack, left_root);
      left = pivot;
      while (left = left.getPrevious()) {
        pushNodeElement(left_child_stack, left.clone(true));
      }

      // Build a queue of children to add to the right-hand partition.
      right = pivot;
      pushNodeElement(right_child_queue, right_root);
      while (right = right.getNext()) {
        pushNodeElement(right_child_queue, right.clone(true));
      }

      pivot = pivot.getParent();
    }

    partitioned.left = readTree(left_root);
    partitioned.right = readTree(right_root);

    return partitioned;
  }

  // CKEDITOR dialog definition.
  return {
    title: 'Add ' + settings.singular,
    minWidth: 300,
    minHeight: 100,
    contents: [
      {
        id: 'tab-basic',
        label: 'Basic Settings',
        elements: [
          {
            type: 'html',
            id: 'description',
            html: '<b style="font-weight: bold;">Select the type of ' + settings.singular.toLowerCase() + ' you wish to add to the page:</b>',
          },
          {
            type: 'select',
            id: 'bundle',
            items: bundles,
            default: $button.attr('data-bundle'),
            onShow: function() {
              updateDescription(this, $button.attr('data-bundle'));
            },
            onChange: function() {
              updateDescription(this, this.getValue());
            },
          },
          {
            type: 'html',
            id: 'description',
            html: '',
          }
        ]
      }
    ],
    onOk: function() {
      var partitioned = partitionTree(editor.getSelection().getRanges()[0]);
      var data = {
        delta: $button.attr('data-delta'),
        paragraphs: [],
      };
      data.paragraphs.push({
        field: $field.attr('data-field'),
        bundle: $button.attr('data-bundle'),
        value: partitioned.left,
      });
      data.paragraphs.push({ bundle: this.getValueOf('tab-basic', 'bundle') });
      if (partitioned.right.length) {
        data.paragraphs.push({
          field: $field.attr('data-field'),
          bundle: $button.attr('data-bundle'),
          value: partitioned.right,
        });
      }
      $form.find('.paragraphs-insert-data:last').val(JSON.stringify(data));
      $button.mousedown();
    },
  };

});
